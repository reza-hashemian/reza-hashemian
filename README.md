### 👋 Hi, I'm  Reza, and welcome to my GitHub bio.

<h3>👨‍💻 &nbsp; About Me</h3>

- 🤔 &nbsp; I love Python and enjoy when coding with python.
- 💪🏻 &nbsp; 1st place in the National Information Technology Olympiad.
- 🎓 &nbsp; Bachelor's degree in network.
- 💼 &nbsp; I like Remote works and Automate tasks.
- 🌱 &nbsp; Learning python to be expert more!

***
# Where can you find me:
<div align='center' style="width: auto;height: 60px;">


<a href="https://www.youtube.com/@learnbytes" style="color: white; border: none; padding: 14px 28px; cursor: pointer;background-color: #428ec1;margin: 5px">Youtube</a>


<a href="https://t.me/official_learn_bytes" style="color: white; border: none; padding: 14px 28px; cursor: pointer;background-color: #428ec1;margin: 5px">Channel Telegram</a>


<a href="https://github.com/reza-hashemian" style="color: white; border: none; padding: 14px 28px; cursor: pointer;background-color: #428ec1;margin: 5px">Github</a>
</div>

<div align='center' style="width: auto;height: 60px;">


<a href="https://instagram.com/learn_bytes?igshid=MzNlNGNkZWQ4Mg==" style="color: white; border: none; padding: 14px 28px; cursor: pointer;background-color: #428ec1;margin: 5px">Instagram</a>


<a href="https://www.linkedin.com/in/rezahashemian" style="color: white; border: none; padding: 14px 28px; cursor: pointer;background-color: #428ec1;margin: 5px">Linkedin</a>
</div>
